﻿using UnityEngine;
using System.Collections;

public class NewRowTrigger : MonoBehaviour {

	// Use this for initialization
    private GameManager gameManager;
    private int counter;
	void Start () {
        counter = 0;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "GridBubble") {
            counter++;
            if (counter == 28) {
                gameManager.SendMessage("GenerateNewRow", SendMessageOptions.DontRequireReceiver);
                counter = counter - 9;
            }
                
        }
    }
}
