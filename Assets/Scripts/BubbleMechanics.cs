﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class BubbleMechanics : MonoBehaviour {
    public enum BubbleState { Blue, Red, Orange, Green, Purple, Gray, Cyan};

    public BubbleState currentBubbleState;
    public GameObject topLeft = null;
    public GameObject topRight = null;
    public GameObject left = null;
    public GameObject right = null;
    public GameObject bottomLeft = null;
    public GameObject bottomRight = null;
    public bool isStatic = false;
    public int bubbleNumbers = 1;
    public bool isShooted = false;
    public bool isInOddRow;

    public List<GameObject> sameAdjcent = new List<GameObject>();

    private float rad;
    private bool newRowTrigger = false;
    private GameManager gameManager;
    
	void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        if (!isShooted && isStatic) {
            rad = GetComponent<CircleCollider2D>().radius;
            AssignAdjacentBubbles();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (isStatic && !gameManager.IsGameOver) {
            transform.position = new Vector2(transform.position.x, transform.position.y - (Time.deltaTime * 0.05f));
        }
        //Debug.Log(this.GetNumberOfBubbles);
	}

    public bool SetNewRowTrigger {
        get {
            return newRowTrigger;
        }
        set {
            newRowTrigger = value;
        }
    }
    public int GetNumberOfBubbles {
        get {
            return this.sameAdjcent.Count();
        }
    }
    void AssignAdjacentBubbles() {
        
        float tmpY = Mathf.Sin(45f) * rad;
        float tmpX = Mathf.Cos(45f) * rad;

        Vector2 topLeftPos = new Vector2(transform.position.x - (2 * tmpX), transform.position.y + (2 * tmpY));
        Vector2 topRightPos = new Vector2(transform.position.x + (2 * tmpX), transform.position.y + (2 * tmpY));
        Vector2 bottomLeftPos = new Vector2(transform.position.x - (2 * tmpX), transform.position.y - (2 * tmpY));
        Vector2 bottomRightPos = new Vector2(transform.position.x + (2 * tmpX), transform.position.y - (2 * tmpY));
        Vector2 leftPos = new Vector2(transform.position.x - (2 * rad), transform.position.y);
        Vector2 rightPos = new Vector2(transform.position.x + (2 * rad), transform.position.y);

        GameObject[] tmpGameObjects = GameObject.FindGameObjectsWithTag("GridBubble");

        for (int i = 0; i < tmpGameObjects.Length; i++)
        {
            if (Vector2.Distance(tmpGameObjects[i].transform.position, topLeftPos) >= 0f && Vector2.Distance(tmpGameObjects[i].transform.position, topLeftPos) <= rad)
            {
                topLeft = tmpGameObjects[i];
                BubbleMechanics tmpBubbleMechanics = topLeft.GetComponent<BubbleMechanics>();
                tmpBubbleMechanics.bottomRight = this.gameObject;
                if (tmpBubbleMechanics.currentBubbleState == this.currentBubbleState) {
                    tmpBubbleMechanics.sameAdjcent = tmpBubbleMechanics.sameAdjcent.Distinct().ToList();

                    foreach (GameObject go in tmpBubbleMechanics.sameAdjcent)
                    {
                        this.sameAdjcent.Add(go);
                    }
                }
            }
            else if (Vector2.Distance(tmpGameObjects[i].transform.position, topRightPos) >= 0f && Vector2.Distance(tmpGameObjects[i].transform.position, topRightPos) <= rad)
            {
                topRight = tmpGameObjects[i];
                BubbleMechanics tmpBubbleMechanics = topRight.GetComponent<BubbleMechanics>();
                tmpBubbleMechanics.bottomLeft = this.gameObject;

                if (tmpBubbleMechanics.currentBubbleState == this.currentBubbleState)
                {
                    tmpBubbleMechanics.sameAdjcent = tmpBubbleMechanics.sameAdjcent.Distinct().ToList();

                    foreach (GameObject go in tmpBubbleMechanics.sameAdjcent)
                    {
                        this.sameAdjcent.Add(go);
                    }
                }
            }
            else if (Vector2.Distance(tmpGameObjects[i].transform.position, bottomLeftPos) >= 0f && Vector2.Distance(tmpGameObjects[i].transform.position, bottomLeftPos) <= rad)
            {
                bottomLeft = tmpGameObjects[i];
                BubbleMechanics tmpBubbleMechanics = bottomLeft.GetComponent<BubbleMechanics>();
                tmpBubbleMechanics.topRight = this.gameObject;
                if (tmpBubbleMechanics.currentBubbleState == this.currentBubbleState)
                {
                    tmpBubbleMechanics.sameAdjcent = tmpBubbleMechanics.sameAdjcent.Distinct().ToList();

                    foreach (GameObject go in tmpBubbleMechanics.sameAdjcent)
                    {
                        this.sameAdjcent.Add(go);
                    }
                }
            }
            else if (Vector2.Distance(tmpGameObjects[i].transform.position, bottomRightPos) >= 0f && Vector2.Distance(tmpGameObjects[i].transform.position, bottomRightPos) <= rad)
            {
                bottomRight = tmpGameObjects[i];
                BubbleMechanics tmpBubbleMechanics = bottomRight.GetComponent<BubbleMechanics>();
                tmpBubbleMechanics.topLeft = this.gameObject;
                if (tmpBubbleMechanics.currentBubbleState == this.currentBubbleState)
                {
                    tmpBubbleMechanics.sameAdjcent = tmpBubbleMechanics.sameAdjcent.Distinct().ToList();

                    foreach (GameObject go in tmpBubbleMechanics.sameAdjcent)
                    {
                        this.sameAdjcent.Add(go);
                    }
                }
            }
            else if (Vector2.Distance(tmpGameObjects[i].transform.position, leftPos) >= 0f && Vector2.Distance(tmpGameObjects[i].transform.position, leftPos) <= rad)
            {
                left = tmpGameObjects[i];
                BubbleMechanics tmpBubbleMechanics = left.GetComponent<BubbleMechanics>();
                tmpBubbleMechanics.right = this.gameObject;
                if (tmpBubbleMechanics.currentBubbleState == this.currentBubbleState)
                {
                    tmpBubbleMechanics.sameAdjcent = tmpBubbleMechanics.sameAdjcent.Distinct().ToList();

                    foreach (GameObject go in tmpBubbleMechanics.sameAdjcent)
                    {
                        this.sameAdjcent.Add(go);
                    }
                }
            }
            else if (Vector2.Distance(tmpGameObjects[i].transform.position, rightPos) >= 0f && Vector2.Distance(tmpGameObjects[i].transform.position, rightPos) <= rad)
            {
                right = tmpGameObjects[i];
                BubbleMechanics tmpBubbleMechanics = right.GetComponent<BubbleMechanics>();
                tmpBubbleMechanics.left= this.gameObject;
                if (tmpBubbleMechanics.currentBubbleState == this.currentBubbleState)
                {
                    tmpBubbleMechanics.sameAdjcent = tmpBubbleMechanics.sameAdjcent.Distinct().ToList();

                    if (isStatic && !isShooted)
                    {
                        this.sameAdjcent.Add(right);
                    }
                    else
                    {
                        foreach (GameObject go in tmpBubbleMechanics.sameAdjcent)
                        {
                            this.sameAdjcent.Add(go);
                        }
                    }
                }
            }
        }
        this.sameAdjcent = this.sameAdjcent.Distinct().ToList();
        foreach(GameObject go in this.sameAdjcent){
            BubbleMechanics bm = go.GetComponent<BubbleMechanics>();
            bm.sameAdjcent = this.sameAdjcent.Distinct().ToList();
            bm.sameAdjcent.Add(this.gameObject);
            bm.sameAdjcent = bm.sameAdjcent.Distinct().ToList();
        }
        this.sameAdjcent.Add(this.gameObject);

        if (isShooted && this.sameAdjcent.Count >= 3) {
            List<GameObject> tmpList = this.sameAdjcent;
            foreach(GameObject go in tmpList){
                Destroy(go);
            }
        }

    }
    void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "GridBubble" && !isStatic) {
            gameObject.tag = "GridBubble";
            rad = coll.gameObject.GetComponent<CircleCollider2D>().radius;
            Vector2 collidedTransform = coll.transform.position;
            Vector2 contactPoint = coll.contacts[0].point;

            transform.rotation = Quaternion.Euler(Vector3.zero);
            Destroy(gameObject.rigidbody2D);
            isStatic = true;

            float tmpY = Mathf.Sin(45f) * rad;
            float tmpX = Mathf.Cos(45f) * rad;

            if (contactPoint.x < collidedTransform.x && contactPoint.y < collidedTransform.y)
            {
                if (contactPoint.x < (collidedTransform.x - tmpX))
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x - (2 * rad), collidedTransform.y);
                    transform.position = tmpPos;
                }
                else
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x - (2 * tmpX), collidedTransform.y - (2 * tmpY));
                    transform.position = tmpPos;
                }
            }
            else if (contactPoint.x >= collidedTransform.x && contactPoint.y < collidedTransform.y)
            {
                if (contactPoint.x > (collidedTransform.x + tmpX))
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x + (2 * rad), collidedTransform.y);
                    transform.position = tmpPos;
                }
                else
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x + (2 * tmpX), collidedTransform.y - (2 * tmpY));
                    transform.position = tmpPos;
                }
            }
            else if (contactPoint.x < collidedTransform.x && contactPoint.y >= collidedTransform.y)
            {
                if (contactPoint.x < (collidedTransform.x - tmpX))
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x - (2 * rad), collidedTransform.y);
                    transform.position = tmpPos;
                }
                else
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x - (2 * tmpX), collidedTransform.y + (2 * tmpY));
                    transform.position = tmpPos;
                }
            }
            else if (contactPoint.x >= collidedTransform.x && contactPoint.y >= collidedTransform.y)
            {
                if (contactPoint.x > (collidedTransform.x + tmpX))
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x + (2 * rad), collidedTransform.y);
                    transform.position = tmpPos;
                }
                else
                {
                    Vector2 tmpPos = new Vector2(collidedTransform.x + (2 * tmpX), collidedTransform.y + (2 * tmpY));
                    transform.position = tmpPos;
                }
            }
            AssignAdjacentBubbles();
            
        }
        
    }

    public void SetType(int rnd) {
        if (rnd == 0)
        {
            currentBubbleState = BubbleState.Blue;
        }
        else if (rnd == 1)
        {
            currentBubbleState = BubbleState.Cyan;
        }
        else if (rnd == 2)
        {
            currentBubbleState = BubbleState.Gray;
        }
        else if (rnd == 3)
        {
            currentBubbleState = BubbleState.Green;
        }
        else if (rnd == 4)
        {
            currentBubbleState = BubbleState.Orange;
        }
        else if (rnd == 5)
        {
            currentBubbleState = BubbleState.Purple;
        }
        else if (rnd == 6)
        {
            currentBubbleState = BubbleState.Red;
        }
    }


}
