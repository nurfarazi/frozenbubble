﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    [SerializeField] private Sprite[] sprites = new Sprite[7];
    [SerializeField] private Vector2 initialBubbleSpawn;
    [SerializeField] private int maxRows;
    public GameObject ball;

    private float rad;
    private bool isGameOver = false;
    private bool isLastIsOdd = false;
	void Start () {
        isGameOver = false;
        rad = ball.GetComponent<CircleCollider2D>().radius;
        bool isLastIsOdd = false;
        GenerateRowsFirstTime();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool IsGameOver {
        get {
            return isGameOver;
        }
    }
    void GameOver() {
        isGameOver = true;
        Application.LoadLevel(0);
    }

    void GenerateNewRow() {
        Debug.Log("New Row Generated");
        Vector2 spawnPoint = initialBubbleSpawn;
        int maxColumn = 10;

        if (isLastIsOdd)
        {
            maxColumn = 10;
            spawnPoint = new Vector2(initialBubbleSpawn.x - rad, initialBubbleSpawn.y);
            isLastIsOdd = false;
            
        }
        else {
            maxColumn = 9;
            Vector2 tmpVector = new Vector2(initialBubbleSpawn.x, initialBubbleSpawn.y);
            spawnPoint = tmpVector;
            isLastIsOdd = true;
        }

        for (int j = 0; j < maxColumn; j++)
        {
            GameObject tmp = Instantiate(ball, spawnPoint, Quaternion.Euler(Vector2.zero)) as GameObject;
            BubbleMechanics tmpBM = tmp.GetComponent<BubbleMechanics>();
            tmp.tag = "GridBubble";
            tmpBM.isStatic = true;
            //Debug.Log(i + " ---- " + maxRows);

            //Destroy(tmp.GetComponent<Rigidbody2D>());

            int rnd = Random.Range(0, 6);
            while (rnd < 0 || rnd > 6)
                rnd = Random.Range(0, 6);

            tmpBM.SetType(rnd);

            if (rnd == 0)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }
            else if (rnd == 1)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }
            else if (rnd == 2)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }
            else if (rnd == 3)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }
            else if (rnd == 4)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }
            else if (rnd == 5)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }
            else if (rnd == 6)
            {
                tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
            }

            Vector2 tmpVector = new Vector2(spawnPoint.x + (2 * rad), spawnPoint.y);
            spawnPoint = tmpVector;
        }
    }

    void GenerateRowsFirstTime() {
        Vector2 spawnPoint = initialBubbleSpawn;
        for (int i = 0; i < maxRows; i++)
        {
            int maxColumn = 10;
            
            if (i % 2 == 0)
            {
                maxColumn = 9;
                if (i == 0)
                {
                    spawnPoint = initialBubbleSpawn;
                }
                else
                {
                    Vector2 tmpVector = new Vector2(initialBubbleSpawn.x, spawnPoint.y - (2 * rad));
                    spawnPoint = tmpVector;
                }
            }
            else {
                maxColumn = 10;
                spawnPoint = new Vector2(initialBubbleSpawn.x - rad, spawnPoint.y - (2 * rad));
                /*
                if (i == 0)
                {
                    spawnPoint = new Vector2(initialBubbleSpawn.x - rad, initialBubbleSpawn.y + (2 * rad));
                }
                else {
                    Vector2 tmpVector = new Vector2(spawnPoint.x + rad, spawnPoint.y + (2 * rad));
                    spawnPoint = tmpVector;
                }
                 * */
            }

            for (int j = 0; j < maxColumn; j++) {
                GameObject tmp = Instantiate(ball, spawnPoint, Quaternion.Euler(Vector2.zero)) as GameObject;
                BubbleMechanics tmpBM = tmp.GetComponent<BubbleMechanics>();
                tmp.tag = "GridBubble";
                if (i == maxRows - 2 && j == 0)
                {
                    tmpBM.SetNewRowTrigger = true;
                    Debug.Log("SetToTrue ------ " + tmpBM.SetNewRowTrigger);
                }
                tmpBM.isStatic = true;
                //Debug.Log(i + " ---- " + maxRows);
                
                //Destroy(tmp.GetComponent<Rigidbody2D>());

                int rnd = Random.Range(0, 6);
                while (rnd < 0 || rnd > 6)
                    rnd = Random.Range(0, 6);
                
                tmpBM.SetType(rnd);
                
                if (rnd == 0)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }
                else if (rnd == 1)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }
                else if (rnd == 2)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }
                else if (rnd == 3)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }
                else if (rnd == 4)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }
                else if (rnd == 5)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }
                else if (rnd == 6)
                {
                    tmp.GetComponent<SpriteRenderer>().sprite = sprites[rnd];
                }

                Vector2 tmpVector = new Vector2(spawnPoint.x + (2 * rad), spawnPoint.y);
                spawnPoint = tmpVector;
            }
            if (i == maxRows - 1)
            {
                if (maxColumn == 9)
                {
                    isLastIsOdd = true;
                }
                else
                {
                    isLastIsOdd = false;
                }

            }
        }
    }
}
