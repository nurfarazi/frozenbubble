﻿using UnityEngine;
using System.Collections;

public class ShooterManager : MonoBehaviour {

	// Use this for initialization
    public Transform spawnPoint;
    public GameObject ball;

    [SerializeField] private Sprite[] sprites = new Sprite[7];

    private int currentBubbleIndex;
    private int nextBubbleIndex;

    private Sprite nextBallSprite = null;
    private Sprite currentBallSprite = null;
    private GameObject nextBall;
    private GameManager gameManager;
    void Start () {
        nextBall = GameObject.Find("NextBall");
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        int rnd = Random.Range(0, 6);
        while (rnd < 0 || rnd > 6)
            rnd = Random.Range(0, 6);
        nextBallSprite = sprites[rnd];
        nextBubbleIndex = rnd;

        rnd = Random.Range(0, 8);
        while (rnd < 0 || rnd > 6)
            rnd = Random.Range(0, 6);
        currentBallSprite = sprites[rnd];
        currentBubbleIndex = rnd;

        nextBall.GetComponent<SpriteRenderer>().sprite = nextBallSprite;
        GetComponent<SpriteRenderer>().sprite = currentBallSprite;

        
	}
	
	// Update is called once per frame
	void Update () {

        

        if (Input.anyKeyDown && !gameManager.IsGameOver) {
            Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            diff.Normalize();

            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

            GameObject tmp = Instantiate(ball, spawnPoint.position, spawnPoint.rotation) as GameObject;
            tmp.GetComponent<SpriteRenderer>().sprite = currentBallSprite;
            tmp.GetComponent<Rigidbody2D>().AddForce(diff * 2500f, ForceMode2D.Force);
            tmp.GetComponent<BubbleMechanics>().isShooted = true;
            if (currentBubbleIndex == 0)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Blue;
            }
            else if (currentBubbleIndex == 1)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Cyan;
            }
            else if (currentBubbleIndex == 2)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Gray;
            }
            else if (currentBubbleIndex == 3)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Green;
            }
            else if (currentBubbleIndex == 4)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Orange;
            }
            else if (currentBubbleIndex == 5)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Purple;
            }
            else if (currentBubbleIndex == 6)
            {
                tmp.GetComponent<BubbleMechanics>().currentBubbleState = BubbleMechanics.BubbleState.Red;
            }
            
            currentBallSprite = nextBallSprite;
            int rnd = Random.Range(0, 6);
            while(rnd < 0 || rnd > 6)
                rnd = Random.Range(0, 6);
            nextBallSprite = sprites[rnd];
            nextBall.GetComponent<SpriteRenderer>().sprite = nextBallSprite;
            GetComponent<SpriteRenderer>().sprite = currentBallSprite;
            currentBubbleIndex = nextBubbleIndex;
            nextBubbleIndex = rnd;
            
        }
	}
}
