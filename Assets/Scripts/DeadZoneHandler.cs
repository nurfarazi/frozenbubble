﻿using UnityEngine;
using System.Collections;

public class DeadZoneHandler : MonoBehaviour {

    private GameManager gameManager;
	void Start () {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(other.gameObject.tag);
        if (other.gameObject.tag == "GridBubble") {
            bool isGameOver = other.gameObject.GetComponent<BubbleMechanics>().isStatic;
            Debug.Log(isGameOver);
            if (isGameOver) {
                gameManager.SendMessage("GameOver", SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
